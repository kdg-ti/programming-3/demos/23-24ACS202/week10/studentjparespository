package be.kdg.programming3.studentjparepository.service;

import be.kdg.programming3.studentjparepository.domain.Student;
import be.kdg.programming3.studentjparepository.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{
    private StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Optional<Student> getStudent(int id) {
        return studentRepository.findById(id);
    }

    @Override
    public void changeStudent(Student student) {
        studentRepository.save(student);
    }

    @Override
    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }
    @Override
    public List<Student> getBigStudents() {
        return studentRepository.findByLengthGreaterThan(200);
    }

    @Override
    public List<Student> getSmallStudents() {
        return studentRepository.findByLengthLessThan(130);
    }

    @Override
    public List<Student> getYoungStudents() {
        LocalDate tenYearsAgo = LocalDate.now().minusYears(10);
        return studentRepository.findByBirthdayAfter(tenYearsAgo);
    }

    @Override
    public List<Student> getOldStudents() {
        LocalDate sixtyYearsAgo = LocalDate.now().minusYears(60);
        return studentRepository.findByBirthdayBefore(sixtyYearsAgo);
    }

    @Override
    public List<Student> getTenFolds() {
        return studentRepository.findByNameEndsWith("0");
    }

    @Override
    public int getNumberOf10Folds() {
        return studentRepository.countByNameEndsWith("0");
    }

    public List<Student> getStudentsWithCredits(){
        return studentRepository.findStudentsWithCredits();
    }


    public List<Student> getAdults(){
        LocalDate date = LocalDate.now().minusYears(18);
        return studentRepository.findStudentsOlderThan(date);
    }

    public List<Student> getDuplicateNamedStudents(){
        return studentRepository.findStudentsWithDuplicateNames();
    }
}
