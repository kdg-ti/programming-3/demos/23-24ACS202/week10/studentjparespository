package be.kdg.programming3.studentjparepository.service;

import be.kdg.programming3.studentjparepository.domain.Student;

import java.util.List;
import java.util.Optional;


public interface StudentService {
    Student addStudent(Student student);
    List<Student> getStudents();
    Optional<Student> getStudent(int id);
    void changeStudent(Student student);
    void deleteStudent(int id);
    List<Student> getBigStudents(); //length>200
    List<Student> getSmallStudents(); //length<140
    List<Student> getYoungStudents(); //younger than 10 years
    List<Student> getOldStudents(); //older than 60 years
    List<Student> getTenFolds(); //name ends with 0
    int getNumberOf10Folds();
    List<Student> getStudentsWithCredits();
    List<Student> getAdults();
    List<Student> getDuplicateNamedStudents();

}
