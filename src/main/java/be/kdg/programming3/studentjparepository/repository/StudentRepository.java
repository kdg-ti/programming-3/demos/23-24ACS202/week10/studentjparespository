package be.kdg.programming3.studentjparepository.repository;

import be.kdg.programming3.studentjparepository.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    List<Student> findByLengthGreaterThan(double length);
    List<Student> findByLengthLessThan(double length);
    List<Student> findByBirthdayAfter(LocalDate date);
    List<Student> findByNameEndsWith(String suffix);
    List<Student> findByBirthdayBefore(LocalDate date);

    int countByNameEndsWith(String suffix);

    @Query("SELECT s FROM Student s WHERE s.credits > 20")
    List<Student> findStudentsWithCredits();

    @Query("SELECT s FROM Student s WHERE s.birthday < :date")
    List<Student> findStudentsOlderThan(LocalDate date);

    @Query(value = "SELECT * FROM STUDENTS s WHERE s.NAME IN " +
            "(SELECT NAME from STUDENTS group by NAME " +
            "HAVING COUNT(NAME)>1) ORDER BY s.NAME", nativeQuery = true)
    List<Student> findStudentsWithDuplicateNames();
}
