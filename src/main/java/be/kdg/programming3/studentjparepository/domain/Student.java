package be.kdg.programming3.studentjparepository.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Random;

@Entity
@Table(name = "STUDENTS")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private double length;
    private LocalDate birthday;
    private int credits;

    protected Student() {
    }

    public static Student getRandom() {
        Random random = new Random();
        return new Student("name" + random.nextInt(1000),
                random.nextDouble(130, 210), LocalDate.of(random.nextInt(1900, 2010),
                random.nextInt(1, 13), random.nextInt(1, 28)), random.nextInt(0, 180));
    }

    public Student(String name, double length, LocalDate birthday, int credits) {
        this.name = name;
        this.length = length;
        this.birthday = birthday;
        this.credits = credits;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", length=" + length +
                ", birthday=" + birthday +
                ", credits=" + credits +
                '}';
    }
}
