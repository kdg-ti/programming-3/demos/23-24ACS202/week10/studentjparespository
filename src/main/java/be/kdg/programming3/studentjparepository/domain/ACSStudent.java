package be.kdg.programming3.studentjparepository.domain;

import jakarta.persistence.Entity;

import java.time.LocalDate;

@Entity
public class ACSStudent extends Student{
    private String country;

    public ACSStudent(String name, double length, LocalDate birthday, int credits, String country) {
        super(name, length, birthday, credits);
        this.country = country;
    }

    public ACSStudent() {

    }

    @Override
    public String toString() {
        return "ACSStudent{" +
                "country='" + country + '\'' +
                '}';
    }
}
