package be.kdg.programming3.studentjparepository.domain;

import jakarta.persistence.Entity;

import java.time.LocalDate;

@Entity
public class FlexStudent extends Student{
    private String occupation;

    public FlexStudent() {
    }

    public FlexStudent(String name, double length, LocalDate birthday, int credits, String occupation) {
        super(name, length, birthday, credits);
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return "FlexStudent{" +
                "occupation='" + occupation + '\'' +
                '}';
    }
}
