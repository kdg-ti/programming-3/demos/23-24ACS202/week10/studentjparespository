package be.kdg.programming3.studentjparepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentjparepositoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentjparepositoryApplication.class, args);
    }

}
