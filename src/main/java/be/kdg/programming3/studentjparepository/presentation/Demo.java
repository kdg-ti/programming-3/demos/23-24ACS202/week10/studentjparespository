package be.kdg.programming3.studentjparepository.presentation;

import be.kdg.programming3.studentjparepository.domain.ACSStudent;
import be.kdg.programming3.studentjparepository.domain.Student;
import be.kdg.programming3.studentjparepository.service.StudentService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.util.stream.Stream;

@Component
public class Demo implements CommandLineRunner {
    private StudentService studentService;

    public Demo(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Adding 100 students...");
        Stream.generate(Student::getRandom)
                .limit(100)
                .forEach(studentService::addStudent);
        System.out.println("Let's update a Student...");
        System.out.println("First, get one:");
        Student student = studentService.getStudent(10).get();
        System.out.println("Found student:" + student);
        System.out.println("Changing some fields for this student:");
        student.setCredits(0);
        student.setName("John");
        System.out.println("Changed student to " + student);
        System.out.println("Saving it...");
        studentService.changeStudent(student);
        System.out.println("What happens if I add a student with an id that does not yet exist?");
        Student student1 = new Student("test",1, LocalDate.now(),1);
        student1.setId(1000);
        studentService.addStudent(student1);
        System.out.println("It wil ignore the id and give it a new one!");
        System.out.println("Big students:");
        studentService.getBigStudents().forEach(System.out::println);
        System.out.println("Small students:");
        studentService.getSmallStudents().forEach(System.out::println);
        System.out.println("Young students:");
        studentService.getYoungStudents().forEach(System.out::println);
        System.out.println("10-folds:");
        studentService.getTenFolds().forEach(System.out::println);
        System.out.println("Number of 10-folds:" + studentService.getNumberOf10Folds());
        System.out.println("Students with credits:");
        studentService.getStudentsWithCredits().forEach(System.out::println);
        System.out.println("Adults:");
        studentService.getAdults().forEach(System.out::println);
        System.out.println("Duplicates:");
        studentService.getDuplicateNamedStudents().forEach(System.out::println);
        System.out.println("Adding an ACS Student:");
        ACSStudent acsStudent = new ACSStudent("Jef",2,LocalDate.now(), 20, "france");
        System.out.println(acsStudent);
        studentService.addStudent(acsStudent);
    }
}
